/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author TAMAR
 */
public class Server {
    
        public static void main(String[] args) throws IOException {
        
            Socket socket = null;
            InputStreamReader inputStreamReader = null;
            BufferedReader bufferedReaderIn = null;
            BufferedReader bufferedReaderOut = null;  
            ServerSocket serverSocket = null;
            
            
            serverSocket = new ServerSocket(1234);          //listen to port 1234

            while (true){

                try{
                        socket = serverSocket.accept();         //allow communication
                        System.out.println("Connected!");
                        inputStreamReader = new InputStreamReader(socket.getInputStream());
                        bufferedReaderIn = new BufferedReader(inputStreamReader);
                        bufferedReaderOut = new BufferedReader(new InputStreamReader(System.in));   //able send msg to client
                        PrintStream printStream = new PrintStream(socket.getOutputStream());        //able get input from keyboard
                    
                    while (true) {
  
                        String inputString, outputString;
                        while ((inputString = bufferedReaderIn.readLine()) != null) {
                            System.out.println("Client: " + inputString);
                            outputString =  bufferedReaderOut.readLine();
                            printStream.println(outputString);

                            if(inputString.equalsIgnoreCase("BYE"))     //end communication
                                break;
                        }
  
                        printStream.close();
                        bufferedReaderIn.close();
                        bufferedReaderOut.close();
                        serverSocket.close();
                        socket.close();   
                }

                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
        }

    }
}
        
    
