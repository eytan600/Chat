/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author TAMAR
 */
public class Client {
    
        public static void main(String[] args) {
            
            Socket socket = null;
            InputStreamReader inputStreamReader = null;
            OutputStreamWriter outputStreamWriter = null;
            BufferedReader bufferedReader = null;
            BufferedWriter bufferedWriter = null;


            
            try{

                socket = new Socket("localhost", 1234);         
                inputStreamReader = new InputStreamReader(socket.getInputStream());
                outputStreamWriter = new OutputStreamWriter(socket.getOutputStream());
                bufferedReader = new BufferedReader(inputStreamReader);
                bufferedWriter = new BufferedWriter(outputStreamWriter);
                
                Scanner scanner = new Scanner(System.in);    //able get input from the user
                
                while (true){
                    
                    String msgToServer = scanner.nextLine();  
                    bufferedWriter.write(msgToServer);      //write to server
                    bufferedWriter.newLine();               
                    bufferedWriter.flush();                 //clean the bufferedWriter
                    
                    System.out.println("Server: " + bufferedReader.readLine());
                    
                    if(msgToServer.equalsIgnoreCase("BYE"))         //end the communication
                        break;
                    
                }
            }
            catch (IOException ex){
                ex.printStackTrace();
            }
            finally{
                
                try{
                    if (socket != null)
                        socket.close();
                    if (inputStreamReader != null)
                        inputStreamReader.close();
                    if (outputStreamWriter != null)
                        outputStreamWriter.close();
                    if (bufferedReader != null)
                        bufferedReader.close();
                    if (bufferedWriter != null)
                        bufferedWriter.close();
                }
                catch (IOException ex){
                    ex.printStackTrace();
                }
            }
    }
}
